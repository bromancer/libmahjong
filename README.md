# Libmahjong
Riichi mahjong library in rust.

[Coverage report](https://bromancer.gitlab.io/libmahjong/cov)
[Code Documentation](https://bromancer.gitlab.io/libmahjong/doc/mahjong/)
[Code Architecture](https://bromancer.gitlab.io/libmahjong/doc/architecture.pdf)
