use tile::Tile;
use super::Set;
use super::Yaku;

pub enum HandState {
    Alive,
    Dead,
}

pub struct Hand<'a> {
    pub concealed_tiles: Vec<&'a Tile>,
    pub sets: Vec<Set<'a>>,
    pub state: HandState,
    pub discarded_tiles: Vec<&'a Tile>,
    pub yaku: Vec<Yaku>,
    pub winning_tile: Option<&'a Tile>,
}

impl<'a> Hand<'a> {
    pub fn is_tenpai() -> bool {
        false
    }
}
