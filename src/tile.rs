use super::WindValue;
use super::Error;
use std::cmp::Ordering;
use std::str::FromStr;

pub trait CyclicItem where Self: Sized{
    fn get_next_wrapping(&self) -> Self;
    fn get_prev_wrapping(&self) -> Self;
    fn get_next(&self) -> Option<Self>;
    fn get_prev(&self) -> Option<Self>;
}

#[derive(Debug, PartialEq, Clone, Copy, Eq, PartialOrd, Ord)]
pub enum SuitValue {
    One,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
}

impl FromStr for SuitValue {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "1" => Ok(SuitValue::One),
            "2" => Ok(SuitValue::Two),
            "3" => Ok(SuitValue::Three),
            "4" => Ok(SuitValue::Four),
            "5" => Ok(SuitValue::Five),
            "6" => Ok(SuitValue::Six),
            "7" => Ok(SuitValue::Seven),
            "8" => Ok(SuitValue::Eight),
            "9" => Ok(SuitValue::Nine),
            _ => Err(Error::ParseTileError),
        }
    }
}

#[test]
fn suit_value_from_str() {
    let x = "7".parse::<SuitValue>();
    assert!(x.is_ok());
    match x {
        Ok(x) => assert_eq!(SuitValue::Seven, x),
        Err(error_code) => panic!("{}", error_code),
    }
}

#[test]
fn break_suit_value_from_str() {
    let x = "x".parse::<SuitValue>();
    assert!(x.is_err());
}

#[derive(Debug, PartialEq, Clone, Copy, Eq, PartialOrd, Ord)]
pub enum DragonValue {
    Red,
    White,
    Green,
}

impl FromStr for DragonValue {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "R" => Ok(DragonValue::Red),
            "W" => Ok(DragonValue::White),
            "G" => Ok(DragonValue::Green),
            _ => Err(Error::ParseTileError),
        }
    }
}

#[derive(Debug, PartialEq, Clone, Copy, Eq, PartialOrd, Ord)]
pub enum TileType {
    Character(SuitValue),
    Pin(SuitValue),
    Bamboo(SuitValue),
    Wind(WindValue),
    Dragon(DragonValue),
}

impl FromStr for TileType {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.len() == 2 {
            let prefix = &s[..1];
            let suffix = &s[1..];
            match prefix {
                "C" => {
                    let v = SuitValue::from_str(suffix)?;
                    Ok(TileType::Character(v))
                }
                "P" => {
                    let v = SuitValue::from_str(suffix)?;
                    Ok(TileType::Pin(v))
                },
                "B" => {
                    let v = SuitValue::from_str(suffix)?;
                    Ok(TileType::Bamboo(v))
                },
                "D" => {
                    let v = DragonValue::from_str(suffix)?;
                    Ok(TileType::Dragon(v))
                },
                "W" => {
                    let v = WindValue::from_str(suffix)?;
                    Ok(TileType::Wind(v))
                },
                _ => Err(Error::ParseTileError),
            }
        } else {
            Err(Error::ParseTileError)
        }
    }
}

impl CyclicItem for SuitValue {
    fn get_next_wrapping(&self) -> SuitValue {
        match self {
            &SuitValue::One => SuitValue::Two,
            &SuitValue::Two => SuitValue::Three,
            &SuitValue::Three => SuitValue::Four,
            &SuitValue::Four => SuitValue::Five,
            &SuitValue::Five => SuitValue::Six,
            &SuitValue::Six => SuitValue::Seven,
            &SuitValue::Seven => SuitValue::Eight,
            &SuitValue::Eight => SuitValue::Nine,
            &SuitValue::Nine => SuitValue::One,
        }
    }

    fn get_prev_wrapping(&self) -> SuitValue {
        match self {
            &SuitValue::One => SuitValue::Nine,
            &SuitValue::Two => SuitValue::One,
            &SuitValue::Three => SuitValue::Two,
            &SuitValue::Four => SuitValue::Three,
            &SuitValue::Five => SuitValue::Four,
            &SuitValue::Six => SuitValue::Five,
            &SuitValue::Seven => SuitValue::Six,
            &SuitValue::Eight => SuitValue::Seven,
            &SuitValue::Nine => SuitValue::Eight,
        }
    }
    fn get_prev(&self) -> Option<SuitValue> {
        match self {
            &SuitValue::One => None,
            _ => Some(self.get_prev_wrapping()),
        }
    }
    fn get_next(&self) -> Option<SuitValue> {
        match self {
            &SuitValue::Nine => None,
            _ => Some(self.get_prev_wrapping()),
        }
    }
}

impl CyclicItem for DragonValue {
    fn get_next_wrapping(&self) -> DragonValue {
        match self {
            &DragonValue::Green => DragonValue::Red,
            &DragonValue::Red => DragonValue::White,
            &DragonValue::White => DragonValue::Green,
        }
    }
    fn get_prev_wrapping(&self) -> DragonValue {
        match self {
            &DragonValue::Green => DragonValue::White,
            &DragonValue::Red => DragonValue::Green,
            &DragonValue::White => DragonValue::Red,
        }
    }
    fn get_prev(&self) -> Option<DragonValue> {
        match self {
            &DragonValue::Green => None,
            _ => Some(self.get_prev_wrapping()),
        }
    }
    fn get_next(&self) -> Option<DragonValue> {
        match self {
            &DragonValue::White => None,
            _ => Some(self.get_prev_wrapping()),
        }
    }
}

impl CyclicItem for WindValue {
    fn get_next_wrapping(&self) -> WindValue {
        match self {
            &WindValue::East => WindValue::South,
            &WindValue::South => WindValue::West,
            &WindValue::West => WindValue::North,
            &WindValue::North => WindValue::East,
        }
    }
    fn get_prev_wrapping(&self) -> WindValue {
        match self {
            &WindValue::East => WindValue::North,
            &WindValue::South => WindValue::East,
            &WindValue::West => WindValue::South,
            &WindValue::North => WindValue::West,
        }
    }
    fn get_prev(&self) -> Option<WindValue> {
        match self {
            &WindValue::East => None,
            _ => Some(self.get_prev_wrapping()),
        }
    }
    fn get_next(&self) -> Option<WindValue> {
        match self {
            &WindValue::North => None,
            _ => Some(self.get_prev_wrapping()),
        }
    }
}

// Here be dragons (no winds or terminals though)
macro_rules! option_cycle_helper {
    ($self:expr, $transform:expr, $flatmap:expr, $wrap:expr) => {{
        match $self {
            &TileType::Character(v) => $flatmap($transform(v),&|v| $wrap(TileType::Character(v))),
            &TileType::Pin(v) => $flatmap($transform(v),&|v| $wrap(TileType::Pin(v))),
            &TileType::Bamboo(v) => $flatmap($transform(v),&|v| $wrap(TileType::Bamboo(v))),
            &TileType::Wind(v) => $flatmap($transform(v),&|v| $wrap(TileType::Wind(v))),
            &TileType::Dragon(v) => $flatmap($transform(v),&|v| $wrap(TileType::Dragon(v))),
        }
    }}
}

#[test]
fn verify_this_monstrosity() {
    let original = TileType::Bamboo(SuitValue::Seven);
    let dummy = option_cycle_helper!(&original, identity, apply, identity);
    assert_eq!(original, dummy);
}

fn get_next_wrapping<T: CyclicItem>(v: T) -> T{
    v.get_next_wrapping()
}
fn get_prev_wrapping<T: CyclicItem>(v: T) -> T{
    v.get_prev_wrapping()
}
fn get_next<T: CyclicItem>(v: T) -> Option<T>{
    v.get_next()
}
fn get_prev<T: CyclicItem>(v: T) -> Option<T>{
    v.get_prev()
}

fn identity<T> (t: T) -> T {
    t
}

fn apply<T, V> (t: T, f: &Fn(T) -> V) -> V {
    f(t)
}

impl CyclicItem for TileType {
    fn get_next_wrapping(&self) -> TileType {
        option_cycle_helper!(self, get_next_wrapping, apply, identity)
    }
    fn get_prev_wrapping(&self) -> TileType {
        option_cycle_helper!(self, get_prev_wrapping, apply, identity)
    }
    fn get_next(&self) -> Option<TileType> {
        option_cycle_helper!(self, get_next, Option::and_then, Option::Some)
    }
    fn get_prev(&self) -> Option<TileType> {
        option_cycle_helper!(self, get_prev, Option::and_then, Option::Some)
    }
}

#[test]
fn verify_cycle_tile_type() {
    let x = TileType::Bamboo(SuitValue::Nine);
    let y = TileType::Bamboo(SuitValue::One);
    assert_eq!(x.get_next_wrapping(), y);
    assert_eq!(y.get_prev_wrapping(), x);
    assert_eq!(y.get_prev(), None);
    assert_eq!(x.get_next(), None);
}

#[derive(Debug, PartialEq, Clone, Copy, Eq)]
pub enum TileState {
    InWall,
    InDeadWall,
    InHand,
    Discarded,
    DiscardedRiichi,
    Stolen,
    Extended,
}

#[derive(Debug, Clone, Eq)]
pub struct Tile {
    pub tile_type: TileType,
    pub state: TileState,
}


impl FromStr for Tile {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let parts: Vec<&str> = s.split("@").collect();

        let tile_type: TileType = parts[0].parse::<TileType>()?;
        Ok(Tile {
            tile_type,
            state: TileState::InWall,
        })
    }
}

pub fn make_tile(s: &str) -> Tile {
    s.parse::<Tile>().expect("Should be a valid tile")
}

impl Ord for Tile {
    fn cmp(&self, other: &Tile) -> Ordering {
        self.tile_type.cmp(&other.tile_type)
    }
}


impl PartialOrd for Tile {
    fn partial_cmp(&self, other: &Tile) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for Tile {
    fn eq(&self, other: &Tile) -> bool {
        return self.tile_type == other.tile_type;
    }
}

impl Tile {
    pub fn new(tile_type: TileType) -> Tile {
        Tile {
            tile_type,
            state: TileState::InWall,
        }
    }
    pub fn han_value(&self, indicators: &Vec<Tile>) -> i32 {
        let mut han_value = 0;
        for indicator in indicators {
            let next = indicator.tile_type.get_next_wrapping();
            if self.tile_type == next {
                han_value += 1;
            }
        }
        return han_value;
    }
}
