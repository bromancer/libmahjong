pub mod tile;
pub mod player;
pub mod hand;

use tile::Tile;
use tile::TileType;
use player::Player;
use hand::Hand;

use std::fmt;
use std::error::Error as StdError;
use std::str::FromStr;

#[derive(Debug)]
pub enum Error {
    YoloError,
    ParseTileError,
    InvalidSetError,
    InvalidDiscardError,
}

#[derive(Debug, PartialEq, Clone, Copy, Eq, PartialOrd, Ord)]
pub enum WindValue {
    East,
    South,
    West,
    North,
}

impl FromStr for WindValue {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "E" => Ok(WindValue::East),
            "S" => Ok(WindValue::South),
            "W" => Ok(WindValue::West),
            "N" => Ok(WindValue::North),
            _ => Err(Error::ParseTileError),
        }
    }
}

pub enum Action<'a: 'b, 'b> {
    Discard(&'a Tile),
    Extend(&'a Tile, &'b Set<'a>),
    Pon(&'a Tile, &'a Player),
    Chi(&'a Tile, &'a Player),
    Kan(&'a Tile, &'a Player),
    ConcealedKan(&'a Tile, &'a Tile, &'a Tile, &'a Tile),
    Ron(&'a Tile, &'a Player),
    Tsumo,
    Riichi,
    TakeWallTile,
}

#[derive(Debug, PartialEq)]
pub enum SetType<'a> {
    Pon([&'a Tile; 3]),
    Chi([&'a Tile; 3]),
    Kan([&'a Tile; 4]),
}

pub struct Set<'a> {
    pub set_type: SetType<'a>,
    pub stolen_from: Option<&'a Player>,
    pub concealed: bool,
}

impl<'a> Set<'a> {
    fn _fu(&self) -> i32 {
        match self.set_type {
            SetType::Pon(ref _tiles) => 0,
            SetType::Chi(ref _tiles) => 0,
            SetType::Kan(ref _tiles) => 0,
        }
    }
}

pub struct Yaku {
    _han: i32,
}

pub struct Game {
    _prevalent: WindValue,
    _round: i32,
    _bonus: i32,
}

pub struct Wall {
    _tiles: Vec<Tile>,
    _dead_wall: Vec<Tile>,
}

impl Wall {
    fn _indicators(_ura: bool) -> Vec<Tile> {
        return vec![];
    }
    fn _tiles_remaining() -> i32 {
        return 0;
    }
    fn _grab_tile() -> Option<Tile> {
        return None;
    }
    fn _grab_replacement() -> Option<Tile> {
        return None;
    }
}

pub struct PartialSet {
    _tiles: [Tile; 2],
    _uke_ire: i32,
    _missing_tiles: Vec<Tile>,
}

pub struct Mahjong<'a> {
    _sets: Vec<Set<'a>>,
    _partial_set: PartialSet,
}

fn _get_all_mahjongs<'a>(hand: &Hand) -> Vec<Mahjong<'a>> {
    let mahjongs: Vec<Mahjong> = vec![];


    let mut chars: Vec<&Tile> = vec![];

    let mut pins: Vec<&Tile> = vec![];
    let mut bamboos: Vec<&Tile> = vec![];
    let mut dragons: Vec<&Tile> = vec![];
    let mut winds: Vec<&Tile> = vec![];

    // Filter all tiles in groups.
    for tile in &hand.concealed_tiles {
        match tile.tile_type {
            TileType::Dragon(_) => dragons.push(tile),
            TileType::Wind(_) => winds.push(tile),
            _ => (),
        }
    }

    // Sort all tiles:
    // Suits:   123456789
    // Winds:   ESWN
    // Dragons: RWG
    chars.sort();
    pins.sort();
    bamboos.sort();
    dragons.sort();
    winds.sort();

    // let char_sets = make_sets(chars);
    // let pin_sets = make_sets(pins);
    // let bamboo_sets = make_sets(bamboos);

    // @TODO: Figure out how to make all possible mahjongs

    return mahjongs;
}

pub fn handle_action<'a: 'b, 'b>(
    player: &Player,
    hand: &mut Hand<'a>,
    action: &Action<'b, 'a>,
) -> Result<(), Error> {
    match action {
        &Action::Discard(tile) => perform_discard(tile, hand),
        &Action::Pon(discarded_tile, from) => perform_pon(player, discarded_tile, hand, from),
        &Action::Kan(discarded_tile, from) => perform_kan(player, discarded_tile, hand, from),
        _ => Err(Error::YoloError),
    }
}

fn perform_discard<'a>(tile: &'a Tile, hand: &mut Hand<'a>) -> Result<(), Error> {
    let concealed = &mut hand.concealed_tiles;
    let discarded = &mut hand.discarded_tiles;

    let removed_tile = concealed._remove_item(&tile);

    match removed_tile {
        Some(t) => discarded.push(t),
        _ => return Err(Error::InvalidDiscardError),
    }

    Ok(())
}

fn same_tiles<'a>(me: &Player, discarded_tile: &'a Tile, hand: &mut Hand<'a>, from: &'a Player, num: usize) -> Result<(), Error> {
    if me == from {
        return Err(Error::InvalidSetError);
    }

    let mut pon_tiles: Vec<&'a Tile> = vec![];
    let tiles = &mut hand.concealed_tiles;

    for tile in tiles.iter() {
        if tile.tile_type != discarded_tile.tile_type {
            continue
        }
        pon_tiles.push(tile);
        if pon_tiles.len() == num-1 {
            break;
        }
    }

    if pon_tiles.len() != num-1 {
        return Err(Error::InvalidSetError);
    }

    pon_tiles.push(discarded_tile);

    let set_type = match num {
        3 => Ok(SetType::Pon([pon_tiles[0], pon_tiles[1], pon_tiles[2]])),
        4 => Ok(SetType::Kan([pon_tiles[0], pon_tiles[1], pon_tiles[2], pon_tiles[3]])),
        _ => Err(Error::InvalidSetError),
    }?;

    for tile in pon_tiles.iter() {
        tiles._remove_item(&tile);
    }

    let set = Set {
        set_type,
        stolen_from: Some(from),
        concealed: false,
    };

    hand.sets.push(set);

    Ok(())
}


fn perform_kan<'a>(me: &Player, discarded_tile: &'a Tile, hand: &mut Hand<'a>, from: &'a Player) -> Result<(), Error> {
    same_tiles(me, discarded_tile, hand, from, 4)
}

fn perform_pon<'a>(me: &Player, discarded_tile: &'a Tile, hand: &mut Hand<'a>, from: &'a Player) -> Result<(), Error> {
    same_tiles(me, discarded_tile, hand, from, 3)
}

trait ItemRemover {
    type Item;
    fn _remove_item(&mut self, item: &Self::Item) -> Option<Self::Item>;
}


impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Error::YoloError => f.write_str("Yolo error"),
            Error::ParseTileError => f.write_str("ParseTileError"),
            Error::InvalidSetError => f.write_str("InvalidSetError"),
            Error::InvalidDiscardError => f.write_str("InvalidDiscardError"),
        }}}

impl<T: PartialEq> ItemRemover for Vec<T> {
    type Item = T;
    fn _remove_item(&mut self, other: &T) -> Option<T> {
        match self.iter().position(|x| *x == *other) {
            Some(pos) => Some(self.remove(pos)),
            None => None,
        }}}

impl StdError for Error {
    fn description(&self) -> &str {
        match *self {
            Error::YoloError => "Yolo not available",
            Error::ParseTileError => "Tile could not be parsed",
            Error::InvalidSetError => "Not a valid set",
            Error::InvalidDiscardError => "Not a valid discard",
        }
    }
}
