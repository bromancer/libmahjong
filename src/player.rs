use super::WindValue;

pub enum PlayerState {
    // It's the player's turn
    Active,
    // The player is after the current player
    NextPlayer,
    // It's not the player's turn and the player will not be the next player
    Pending,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Player {
    pub points: i32,
    pub seat: WindValue,
}
