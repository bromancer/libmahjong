extern crate mahjong;

use mahjong::tile::Tile;
use mahjong::tile::SuitValue;
use mahjong::tile::TileType;
use mahjong::tile::DragonValue;
use mahjong::WindValue;

#[test]
fn sort_suit_characters() {
    let tile1 = Tile::new(TileType::Character( SuitValue::Nine));
    let tile2 = Tile::new(TileType::Character( SuitValue::Five));
    let tile3 = Tile::new(TileType::Character( SuitValue::Six));
    let tile4 = Tile::new(TileType::Character( SuitValue::Three));

    let mut vec = vec![&tile1, &tile2, &tile3, &tile4];

    vec.sort();

    assert_eq!(vec, [&tile4, &tile2, &tile3, &tile1]);
}

#[test]
fn sort_suit_bamboos() {
    let tile1 = Tile::new(TileType::Bamboo(SuitValue::Nine));
    let tile2 = Tile::new(TileType::Bamboo(SuitValue::Five));
    let tile3 = Tile::new(TileType::Bamboo(SuitValue::Six));
    let tile4 = Tile::new(TileType::Bamboo(SuitValue::Three));

    let mut vec = vec![&tile1, &tile2, &tile3, &tile4];

    vec.sort();

    assert_eq!(vec, [&tile4, &tile2, &tile3, &tile1]);
}

#[test]
fn sort_suit_pins() {
    let tile1 = Tile::new(TileType::Pin(SuitValue::Eight));
    let tile2 = Tile::new(TileType::Pin(SuitValue::Four));
    let tile3 = Tile::new(TileType::Pin(SuitValue::Six));
    let tile4 = Tile::new(TileType::Pin(SuitValue::One));

    let mut vec = vec![&tile1, &tile2, &tile3, &tile4];

    vec.sort();

    assert_eq!(vec, [&tile4, &tile2, &tile3, &tile1]);
}

#[test]
fn sort_type() {
    let pin_tile = Tile::new(TileType::Pin(SuitValue::One));
    let wind_tile = Tile::new(TileType::Wind(WindValue::East));
    let dragon_tile = Tile::new(TileType::Dragon(DragonValue::Green));
    let bamboo_tile = Tile::new(TileType::Bamboo(SuitValue::One));
    let char_tile = Tile::new(TileType::Character( SuitValue::One));

    let mut vec = vec![
        &pin_tile,
        &wind_tile,
        &dragon_tile,
        &bamboo_tile,
        &char_tile,
    ];

    vec.sort();

    assert_eq!(
        vec,
        [
            &char_tile,
            &pin_tile,
            &bamboo_tile,
            &wind_tile,
            &dragon_tile
        ]
    );
}

#[test]
fn sort_winds() {
    let east_tile = Tile::new(TileType::Wind(WindValue::East));
    let north_tile = Tile::new(TileType::Wind(WindValue::North));
    let west_tile = Tile::new(TileType::Wind(WindValue::West));
    let south_tile = Tile::new(TileType::Wind(WindValue::South));

    let mut vec = vec![&east_tile, &north_tile, &west_tile, &south_tile];

    vec.sort();

    assert_eq!(vec, [&east_tile, &south_tile, &west_tile, &north_tile]);
}
