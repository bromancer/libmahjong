extern crate mahjong;

use mahjong::Action;
use mahjong::player::Player;
use mahjong::handle_action;
use mahjong::hand::Hand;
use mahjong::hand::HandState;
use mahjong::tile::make_tile;
use mahjong::WindValue;

const PLAYER: Player = Player {
    points: 0,
    seat: WindValue::East,
};

#[test]
fn discard_tile_from_hand() {
    let tile = make_tile("C1");
    let discard_action = Action::Discard(&tile);
    let mut hand = Hand {
        concealed_tiles: vec![&tile],
        sets: vec![],
        state: HandState::Alive,
        discarded_tiles: vec![],
        yaku: vec![],
        winning_tile: None,
    };

    let original_discarded = hand.discarded_tiles.len();
    let original_concealed = hand.concealed_tiles.len();

    let action = handle_action(&PLAYER, &mut hand, &discard_action);
    assert!(action.is_ok());
    assert_eq!(original_concealed - 1, hand.concealed_tiles.len());
    assert_eq!(original_discarded + 1, hand.discarded_tiles.len());
    assert_eq!(*hand.discarded_tiles[0], tile);
}

#[test]
fn discard_fake_tile() {
    let tile = make_tile("C1");
    let discard_action = Action::Discard(&tile);
    let mut hand = Hand {
        concealed_tiles: vec![],
        sets: vec![],
        state: HandState::Alive,
        discarded_tiles: vec![],
        yaku: vec![],
        winning_tile: None,
    };

    let action = handle_action(&PLAYER, &mut hand, &discard_action);
    assert!(action.is_err());
}

#[test]
fn claim_legitimate_chi() {
    // let bamboo1 = Tile::new(TileType::Suit(TileSuit::Bamboo, SuitValue::One));
    // let bamboo2 = Tile::new(TileType::Suit(TileSuit::Bamboo, SuitValue::Two));
    // let bamboo3 = Tile::new(TileType::Suit(TileSuit::Bamboo, SuitValue::Three));
    //let discard_action = Action::Discard(&tile);
    assert!(true);
}

#[test]
fn claim_illegitimate_chi() {
    assert!(true);
}
