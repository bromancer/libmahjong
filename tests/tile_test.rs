extern crate mahjong;

use mahjong::tile::Tile;
use mahjong::tile::SuitValue;
use mahjong::tile::TileType;
use mahjong::tile::TileState;
use mahjong::tile::DragonValue;
use mahjong::WindValue;

#[test]
fn create_character_one_tile() {
    let tile = Tile::new(TileType::Character( SuitValue::One));

    match tile.tile_type {
        TileType::Character(SuitValue::One) => (),
        _ => panic!("Invalid tile type"),
    }

    match tile.state {
        TileState::InWall => (),
        _ => panic!("Invalid default tile state"),
    }
}

#[test]
fn dora_indicator_han_value() {
    let tile = Tile::new(TileType::Character(SuitValue::Nine));

    let indicators: Vec<Tile> = vec![
        Tile::new(TileType::Character(SuitValue::Eight)),
    ];

    let expected = 1;
    let actual = tile.han_value(&indicators);

    assert_eq!(expected, actual);
}

#[test]
fn double_dora_indicator_han_value() {
    let tile = Tile::new(TileType::Character(SuitValue::Nine));

    let indicators: Vec<Tile> = vec![
        Tile::new(TileType::Character(SuitValue::Eight)),
        Tile::new(TileType::Character(SuitValue::Eight)),
    ];

    let expected = 2;
    let actual = tile.han_value(&indicators);

    assert_eq!(expected, actual);
}

#[test]
fn dora_indicator_turn_around_han_value() {
    let tile = Tile::new(TileType::Character(SuitValue::One));

    let indicators: Vec<Tile> = vec![
        Tile::new(TileType::Character(SuitValue::Nine)),
    ];

    let expected = 1;
    let actual = tile.han_value(&indicators);

    assert_eq!(expected, actual);
}

#[test]
fn dora_indicator_red_dragon_value() {
    let tile = Tile::new(TileType::Dragon(DragonValue::Red));

    let indicators: Vec<Tile> = vec![Tile::new(TileType::Dragon(DragonValue::Green))];

    let expected = 1;
    let actual = tile.han_value(&indicators);

    assert_eq!(expected, actual);
}

#[test]
fn dora_indicator_green_dragon_value() {
    let tile = Tile::new(TileType::Dragon(DragonValue::Green));

    let indicators: Vec<Tile> = vec![Tile::new(TileType::Dragon(DragonValue::White))];

    let expected = 1;
    let actual = tile.han_value(&indicators);

    assert_eq!(expected, actual);
}

#[test]
fn dora_indicator_white_dragon_value() {
    let tile = Tile::new(TileType::Dragon(DragonValue::White));

    let indicators: Vec<Tile> = vec![Tile::new(TileType::Dragon(DragonValue::Red))];

    let expected = 1;
    let actual = tile.han_value(&indicators);

    assert_eq!(expected, actual);
}

#[test]
fn dora_indicator_east_wind_value() {
    let tile = Tile::new(TileType::Wind(WindValue::East));

    let indicators: Vec<Tile> = vec![Tile::new(TileType::Wind(WindValue::North))];

    let expected = 1;
    let actual = tile.han_value(&indicators);

    assert_eq!(expected, actual);
}

#[test]
fn dora_indicator_south_wind_value() {
    let tile = Tile::new(TileType::Wind(WindValue::South));

    let indicators: Vec<Tile> = vec![Tile::new(TileType::Wind(WindValue::East))];

    let expected = 1;
    let actual = tile.han_value(&indicators);

    assert_eq!(expected, actual);
}

#[test]
fn dora_indicator_west_wind_value() {
    let tile = Tile::new(TileType::Wind(WindValue::West));

    let indicators: Vec<Tile> = vec![Tile::new(TileType::Wind(WindValue::South))];

    let expected = 1;
    let actual = tile.han_value(&indicators);

    assert_eq!(expected, actual);
}

#[test]
fn dora_indicator_north_wind_value() {
    let tile = Tile::new(TileType::Wind(WindValue::North));

    let indicators: Vec<Tile> = vec![Tile::new(TileType::Wind(WindValue::West))];

    let expected = 1;
    let actual = tile.han_value(&indicators);

    assert_eq!(expected, actual);
}
