extern crate mahjong;

use mahjong::hand::Hand;
use mahjong::hand::HandState;
use mahjong::tile::*;
use mahjong::player::Player;
use mahjong::WindValue;
use mahjong::*;

const PLAYER_EAST: Player = Player {
    points: 0,
    seat: WindValue::East,
};

const PLAYER_WEST: Player = Player {
    points: 0,
    seat: WindValue::West,
};

#[test]
fn pon_tile_set() {
    let tile1 = make_tile("B6");
    let tile2 = make_tile("B6");
    let discarded_tile = make_tile("B6");

    let mut hand = Hand {
        concealed_tiles: vec![&tile1, &tile2],
        sets: vec![],
        state: HandState::Alive,
        discarded_tiles: vec![],
        yaku: vec![],
        winning_tile: None,
    };

    let pon_action = Action::Pon(&discarded_tile, &PLAYER_WEST);
    let action = handle_action(&PLAYER_EAST, &mut hand, &pon_action);

    assert!(action.is_ok());

    assert_eq!(hand.sets.len(), 1); // Created 1 new set
    assert_eq!(hand.concealed_tiles.len(), 0); // Concealed tiles is empty
    
    let set = &hand.sets[0];

    assert_eq!(set.set_type, SetType::Pon([&tile1, &tile2, &discarded_tile]));
    assert_eq!(set.concealed, false);
    
    match set.stolen_from {
        Some(&PLAYER_WEST) => (),
        Some(_) => panic!("Set stolen from wrong player"),
        _ => panic!("Set not marked as stolen"),
    }
}

#[test]
fn pon_tile_no_set() {
    let tile1 = make_tile("B6");
    let tile2 = make_tile("B6");
    let discarded_tile = make_tile("B7");

    let mut hand = Hand {
        concealed_tiles: vec![&tile1, &tile2],
        sets: vec![],
        state: HandState::Alive,
        discarded_tiles: vec![],
        yaku: vec![],
        winning_tile: None,
    };

    let pon_action = Action::Pon(&discarded_tile, &PLAYER_EAST);
    let action = handle_action(&PLAYER_EAST, &mut hand, &pon_action);

    assert!(action.is_err());
}
